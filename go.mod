module gitlab.alpinelinux.org/alpine/go

go 1.22.0

toolchain go1.23.4

require (
	github.com/MakeNowJust/heredoc/v2 v2.0.1
	github.com/stretchr/testify v1.10.0
	golang.org/x/exp v0.0.0-20250106191152-7588d65b2ba8
	gopkg.in/ini.v1 v1.67.0
	gopkg.in/yaml.v3 v3.0.1
	mvdan.cc/sh/v3 v3.10.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/term v0.25.0 // indirect
)
