package repository

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPackageReturnsProperFilename(t *testing.T) {
	pkg := Package{
		Name:    "test-package",
		Version: "1.2.3-r0",
	}

	assert.Equal(t, "test-package-1.2.3-r0.apk", pkg.Filename())
}

func TestParsePackage(t *testing.T) {
	f, err := os.Open("testdata/hello-0.1.0-r0.apk")
	require.Nil(t, err)
	apk, err := ParsePackage(f)
	require.Nil(t, err)
	require.Equal(t, "hello", apk.Name)
	require.Equal(t, "0.1.0-r0", apk.Version)
	require.Equal(t, "x86_64", apk.Arch)
	require.Equal(t, "just a test package", apk.Description)
	require.Equal(t, "Apache-2.0", apk.License)
	require.Equal(t, []string{"busybox"}, apk.Dependencies)
	require.Equal(t, uint64(499), apk.Size)
	require.Equal(t, uint64(4117), apk.InstalledSize)
	require.Equal(t, "Q1DNWZeWkviN7MJedLpYM8yBvmnGM=", apk.ChecksumString())
	require.Equal(t, []byte{
		0xc, 0xd5, 0x99, 0x79, 0x69, 0x2f, 0x88, 0xde, 0xcc,
		0x25, 0xe7, 0x4b, 0xa5, 0x83, 0x3c, 0xc8, 0x1b, 0xe6,
		0x9c, 0x63}, apk.Checksum)
}

func TestParsePackageWithBuildDate(t *testing.T) {
	f, err := os.Open("testdata/hello-wolfi-2.12.1-r0.apk")
	require.Nil(t, err)
	apk, err := ParsePackage(f)
	require.Nil(t, err)
	require.Equal(t, "hello-wolfi", apk.Name)
	require.Equal(t, "2.12.1-r0", apk.Version)
	require.Equal(t, "x86_64", apk.Arch)
	require.Equal(t, "the GNU hello world program", apk.Description)
	require.Equal(t, "GPL-3.0-or-later", apk.License)
	require.Equal(t, time.Unix(12345678, 0).UTC(), apk.BuildTime)
	require.Equal(t, []string{"so:ld-linux-x86-64.so.2", "so:libc.so.6"}, apk.Dependencies)
	require.Equal(t, uint64(0x11c57), apk.Size)
	require.Equal(t, uint64(0x9c45b), apk.InstalledSize)
	require.Equal(t, "Q1j9huCmxqWKDR+abKskcY8e/aZMo=", apk.ChecksumString())
	require.Equal(t, []byte{
		0x8f, 0xd8, 0x6e, 0xa, 0x6c, 0x6a, 0x58, 0xa0, 0xd1, 0xf9,
		0xa6, 0xca, 0xb2, 0x47, 0x18, 0xf1, 0xef, 0xda, 0x64, 0xca,
	}, apk.Checksum)
}
