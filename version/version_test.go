package version

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersionTokenization(t *testing.T) {
	v := Version("12.34.56.78-r0")

	nt, nv, q := v.GetToken(Digit)

	assert.Equal(t, nt, Token(DigitOrZero))
	assert.Equal(t, int64(12), q)

	nt, nv, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(DigitOrZero))
	assert.Equal(t, int64(34), q)

	nt, nv, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(DigitOrZero))
	assert.Equal(t, int64(56), q)

	nt, nv, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(RevisionDigit))
	assert.Equal(t, int64(78), q)

	nt, _, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(End))
	assert.Equal(t, int64(0), q)

	v = Version("1.1.3i-r0")

	nt, nv, q = v.GetToken(Digit)

	assert.Equal(t, nt, Token(DigitOrZero))
	assert.Equal(t, int64(1), q)

	nt, nv, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(DigitOrZero))
	assert.Equal(t, int64(1), q)

	nt, nv, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(Letter))
	assert.Equal(t, int64(3), q)

	nt, nv, q = nv.GetToken(nt)

	assert.Equal(t, nt, Token(RevisionDigit))
	assert.Equal(t, int64(105), q)

	nt, _, q = nv.GetToken(nt)

	assert.Equal(t, nt, Token(End))
	assert.Equal(t, int64(0), q)

	v = Version("1.1_pre2-r0")

	nt, nv, q = v.GetToken(Digit)

	assert.Equal(t, nt, Token(DigitOrZero))
	assert.Equal(t, int64(1), q)

	nt, nv, q = nv.GetToken(Digit)

	assert.Equal(t, nt, Token(Suffix))
	assert.Equal(t, int64(1), q)

	nt, nv, q = nv.GetToken(Suffix)

	assert.Equal(t, nt, Token(SuffixDigit))
	assert.Equal(t, int64(-2), q)

	nt, nv, q = nv.GetToken(SuffixDigit)

	assert.Equal(t, nt, Token(RevisionDigit))
	assert.Equal(t, int64(2), q)

	nt, _, q = nv.GetToken(RevisionDigit)

	assert.Equal(t, nt, Token(End))
	assert.Equal(t, int64(0), q)
}
